#include "cuda_runtime.h"
#include "device_launch_parameters.h"


#include <cstdio>
#include <math.h>
#include <chrono>

__global__ void akf(int *amplitude, int *code, int *start_count, int *b) {
        int bits = *b; // количество бит

        __shared__ int akf[512]; //текущая атф последовательность для числа (blockId.x)
        __shared__ int binaryNum[512]; // двоичное представление числа
        int buffer = 0;
        int id = 0;
        ////////////////////////////////////////////////////////////////////////////////
        int k = 0; // отвечает за то, какую акф функцию обрабатывает каждая нить 
        if(threadIdx.x < bits){//в данном условии мы разграничим нити на 2 числа
            id = *start_count + (8 * blockIdx.x + 1); // текущее число
            buffer = bool((1 << (bits - 1 - threadIdx.x)) & id);
            
        }else if(threadIdx.x < 2*bits){
            k = bits;
            id = *start_count + (8 * blockIdx.x + 2);
            buffer = bool((1 << (2 * bits - 1 - threadIdx.x)) & id);
        }else if(threadIdx.x < 3*bits){
            k = 2*bits;
            id = *start_count + (8 * blockIdx.x + 3);
            buffer = bool((1 << (3 * bits - 1 - threadIdx.x)) & id);
        }else if(threadIdx.x < 4*bits){
            k = 3*bits;
            id = *start_count + (8 * blockIdx.x + 4);
            buffer = bool((1 << (4 * bits - 1 - threadIdx.x)) & id);
        }else if(threadIdx.x < 5*bits){
            k = 4*bits;
            id = *start_count + (8 * blockIdx.x + 5);
            buffer = bool((1 << (5 * bits - 1 - threadIdx.x)) & id);
        }else if(threadIdx.x < 6*bits){
            k = 5*bits;
            id = *start_count + (8 * blockIdx.x + 6);
            buffer = bool((1 << (6 * bits - 1 - threadIdx.x)) & id);
        }else if(threadIdx.x < 5*bits){
            k = 6*bits;
            id = *start_count + (8 * blockIdx.x + 7);
            buffer = bool((1 << (7 * bits - 1 - threadIdx.x)) & id);
        }else{
            k = 7*bits;
            id = *start_count + (8 * blockIdx.x + 8);
            buffer = bool((1 << (8 * bits - 1 - threadIdx.x)) & id);

        }
        //бинаризируем число с помощью битовых операций, каждая нить записывает свой бит
        binaryNum[threadIdx.x] = -1 + 2*buffer;
        
        __syncthreads(); //синхронизируем, для того что бы бинарное число было представлено полностью
        ////////////////////////////////////////////////////////////////////////////////
        
        int summ = 0; // текущая сумма последовательности смещения по биту  
        //далее запускаем цикл подсчета акф 
        for(int j = k; j < bits; j++){
            if(j + threadIdx.x < bits){
                summ += binaryNum[threadIdx.x + j] * binaryNum[j];
            }
        }
        ////////////////////////////////////////////////////////////////////////////////
        akf[threadIdx.x] = summ; // записываем текущее количество единичек для текущего бита акф
        __syncthreads();//синхронизируем, что бы акф была полной
        ////////////////////////////////////////////////////////////////////////////////
        
        if(threadIdx.x == k){//анализ числа будет проводить в 0 треде
            int first = 1;//переменная с максимум от 2 элемента для числа
            ////////////////////////////////////////////////////////
            
            //цикл для нахождения 2 максимума акф для первого числа
            for(int i = k+1; i < k+bits; i++){
                first = max(first, abs(akf[i]));
            }
            ////////////////////////////////////////////////////////
            
            //цикл для нахождения 2 максимума акф для второго числа
            ////////////////////////////////////////////////////////
            
            int result = 0; //переменная для выявления наилучшей акф
                result = akf[k]/first;
            int buff = atomicMax(amplitude, result); //атомарная функция для записи максимального отношения мощности сигнала и 2 максимума, возвращает старое значение
            if(buff <= result){//если возвращенное значение меньше текущего максимума, значит атомарная функция перезаписала значение, тоже самое делаем и с кодом
                atomicExch(code, id);
            }
            
            ///////////////////////////////////////////////////////
        }
}

int main() {
    for(int j = 5; j <= 32; j++){//цикл для поиска лучших сигналов для конкретного количества бит
        
        int bits = j; //для удобства количество бит
        int numbers = (1 << j); //для удобства количество чисел от 0 до максимума по количеству бит для перебора

        int start_count = (1 << (bits-1)) | (1 << (bits-2)) | (1 << (bits-3));//переменная для оптимизации, мы берем все числа начиная от 111
        
        int amplitude;//переменная где хранится лучшая амплитуда для текущего количества бит
        int code; //переменная где храниться лучший код по версии акф
        //тоесть здесь мы увидим какая битовая последовательность будет наилучшая
        ////////////////////////////////////////////////////////////////////////////////
        
        //выделяем видео память
        int* dev_amplitude;
        int* dev_code;
        int* dev_start_count;
        int* dev_bits;

        cudaMalloc((void**)&dev_amplitude,  sizeof(int));
        cudaMalloc((void**)&dev_code,  sizeof(int));
        cudaMalloc((void**)&dev_start_count,  sizeof(int));
        cudaMalloc((void**)&dev_bits,  sizeof(int));

        cudaMemcpy(dev_start_count, &start_count, sizeof(int), cudaMemcpyHostToDevice);//копируем на видеокарту количество бит, и с какого числа мы стартуем перебор
        cudaMemcpy(dev_bits, &bits, sizeof(int), cudaMemcpyHostToDevice);

        ////////////////////////////////////////////////////////////////////////////////   
        //объявляем количество блоков, и количество нитей для блока
        dim3 threads = dim3(8*bits);
        dim3 blocks = dim3((numbers-start_count)/8+1);
        ////////////////////////////////////////////////////////////////////////////////
        
        //запускаем ядро
        auto start = std::chrono::high_resolution_clock::now();
        akf <<< blocks, threads >>> (dev_amplitude, dev_code, dev_start_count, dev_bits);
        ////////////////////////////////////////////////////////////////////////////////
        
        //копируем из видео памяти в обычную
        cudaMemcpy(&amplitude, dev_amplitude, sizeof(int), cudaMemcpyDeviceToHost);
        cudaMemcpy(&code, dev_code, sizeof(int), cudaMemcpyDeviceToHost);
        ////////////////////////////////////////////////////////////////////////////////
        
        auto end = std::chrono::high_resolution_clock::now();//замер времени
        long duration = std::chrono::duration<double, std::micro>(end - start).count();//продолжительность времени

        ////////////////////////////////////////////////////////////////////////////////
        //вывод
        printf("%*c", (65-(2*bits)),' ');

        for(int i = 0; i < bits; i++){
            printf("%d ", bool((1<<(bits-i-1))&code));
        }

        printf(" - %d ", bits/amplitude);
        printf(" -bits %d ", bits);
        printf(" - %ld microseconds\n ", duration);

        ////////////////////////////////////////////////////////////////////////////////
    }
    return 0;
}
